package store.taotao.jasypt.osgi.config;

import lombok.extern.slf4j.Slf4j;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedServiceFactory;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * jasypt 配置的激活器
 */
@Slf4j
public class Activator implements BundleActivator {


    /**
     * StringEncryptor 配置监听服务
     */
    private StringEncryptorConfigManager configManager;
    /**
     * 主服务注册器，StringEncryptor 配置监听器
     */
    private ServiceRegistration<ManagedServiceFactory> registration;
    @Override
    public void start(BundleContext bundleContext) {
        log.info("jasypt-config start");
        configManager=new StringEncryptorConfigManager(bundleContext);
        Dictionary<String, String> props = new Hashtable<>();
        props.put(Constants.SERVICE_PID,StringEncryptorConstants.FACTORY_PID);
        registration=bundleContext.registerService(ManagedServiceFactory.class,configManager,props);
    }

    @Override
    public void stop(BundleContext bundleContext) {
        log.info("jasypt-config stop");
        registration.unregister();
        configManager.destroy();

    }
}
