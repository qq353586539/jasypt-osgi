package store.taotao.jasypt.osgi.config;

import lombok.extern.slf4j.Slf4j;
import org.jasypt.encryption.StringEncryptor;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * StringEncryptor 配置监听器
 */
@Slf4j
public class StringEncryptorConfigManager implements ManagedServiceFactory {
    private final Map<String, StringEncryptorRegistration> trackers;
    /**
     * bundle 上下文
     */
    private final BundleContext context;

    public StringEncryptorConfigManager(BundleContext context) {
        this.context = context;
        this.trackers = new HashMap<>();
    }

    /**
     * 配置名称
     *
     * @return 配置名称
     */
    @Override
    public String getName() {
        return "stringEncryptor";
    }

    @Override
    public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
        log.info("检测到配置变更 pid=[{}]", pid);
        log.debug("properties=[{}]", properties);
        StringEncryptorConfiguration configuration = new StringEncryptorConfiguration(pid, properties);
        StringEncryptor encryptor = configuration.generate();
        if (null==encryptor){
            log.warn("生成加密器失败 configuration=[{}]",configuration);
            throw new ConfigurationException((String)properties.get("stringEncryptorClass"),"生成加密器失败");
        }
        doDelete(pid);
        Dictionary<String,String> param=new Hashtable<>();
        param.put("component.name",configuration.getServiceName());
        param.put("bean.name",configuration.getServiceName());
        param.put("alias",configuration.getServiceName());
        ServiceRegistration<StringEncryptor> registration = context.registerService(StringEncryptor.class, encryptor, param);
        StringEncryptorRegistration tracker = new StringEncryptorRegistration(registration);
        log.info("添加加密器 tracker=[{}]",tracker);
        trackers.put(pid,tracker);
    }

    /**
     * 销毁 pid 对应的服务
     *
     * @param pid the PID of the service to be removed
     */
    @Override
    public synchronized void deleted(String pid) {
        log.info("检测到配置删除 pid=[{}]", pid);
        doDelete(pid);
    }

    /**
     * 具体的销毁 StringEncryptor 的服务
     * @param pid 要删除的 service 的 pid
     */
    private void doDelete(String pid) {
        try(StringEncryptorRegistration tracker = trackers.remove(pid)) {
            log.info("销毁 StringEncryptor pid=[{}] 成功", pid);
        } catch (Exception e) {
            log.info("销毁 StringEncryptor pid=[{}] 失败", pid,e);
        }

    }


    /**
     * 将所有的服务进行整体销毁。
     */
    synchronized void destroy() {
        log.info("销毁全部 StringEncryptor");
        for (String pid: trackers.keySet()) {
            doDelete(pid);
        }
    }
}
