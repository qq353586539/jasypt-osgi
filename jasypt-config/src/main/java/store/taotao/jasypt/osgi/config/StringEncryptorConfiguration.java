package store.taotao.jasypt.osgi.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.jasypt.commons.CommonUtils;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.jasypt.iv.IvGenerator;
import org.jasypt.iv.RandomIvGenerator;
import org.jasypt.salt.RandomSaltGenerator;
import org.jasypt.salt.SaltGenerator;

import static org.apache.commons.collections4.MapUtils.*;

import java.util.*;

/**
 * 配置项
 */
@Setter
@Getter
@ToString
@Slf4j
public class StringEncryptorConfiguration {

    /**
     * stringEncryptor 的最终实现类
     */
    private Class<? extends StringEncryptor> stringEncryptorClass= PooledPBEStringEncryptor.class;

    /**
     * 服务名
     */
    private String serviceName;

    /**
     * 加解密主密码
     *
     * @see org.jasypt.encryption.pbe.PBEStringEncryptor
     * @see org.jasypt.encryption.pbe.config.StringPBEConfig#getPassword()
     */
    private String password;

    /**
     * 加解密算法
     * <a href="http://www.jasypt.org/cli.html"/>Jasypt CLI Tools Page</a>. 默认值 {@code "PBEWITHHMACSHA512ANDAES_256"}.
     *
     * @see org.jasypt.encryption.pbe.PBEStringEncryptor
     * @see org.jasypt.encryption.pbe.config.StringPBEConfig#getAlgorithm()
     */
    private String algorithm = "PBEWITHHMACSHA512ANDAES_256";

    /**
     * 获取签名时迭代次数. 默认值为 {@code "1000"}.
     *
     * @see org.jasypt.encryption.pbe.PBEStringEncryptor
     * @see org.jasypt.encryption.pbe.config.StringPBEConfig#getKeyObtentionIterations()
     */
    private String keyObtentionIterations = "1000";

    /**
     * 加解密器池的规模. 默认值为 {@code "1"}.
     *
     * @see org.jasypt.encryption.pbe.PBEStringEncryptor
     * @see org.jasypt.encryption.pbe.config.StringPBEConfig#getPoolSize()
     */
    private String poolSize = "1";

    /**
     * {@link java.security.Provider} 加解密器的实现名称. 默认值为 {@code null}.
     *
     * @see org.jasypt.encryption.pbe.PBEStringEncryptor
     * @see org.jasypt.encryption.pbe.config.StringPBEConfig#getProviderName()
     */
    private String providerName;

    /**
     * {@link org.jasypt.salt.SaltGenerator} 盐生成器. 默认值为
     * {@code "org.jasypt.salt.RandomSaltGenerator"}.
     *
     * @see org.jasypt.encryption.pbe.PBEStringEncryptor
     * @see org.jasypt.encryption.pbe.config.StringPBEConfig#getSaltGenerator()
     */
    private Class<? extends SaltGenerator> saltGeneratorClass = RandomSaltGenerator.class;

    /**
     * {@link org.jasypt.iv.IvGenerator} 初始化向量生成器. 默认值为
     * {@code "org.jasypt.iv.RandomIvGenerator"}.
     *
     * @see org.jasypt.encryption.pbe.PBEStringEncryptor
     * @see org.jasypt.encryption.pbe.config.StringPBEConfig#getIvGenerator()
     */
    private Class<? extends IvGenerator> ivGeneratorClass = RandomIvGenerator.class;

    /**
     * 密文输出编码. {@code "base64"} or {@code "hexadecimal"}. 默认值为 {@code "base64"}.
     *
     * @see org.jasypt.encryption.pbe.PBEStringEncryptor
     * @see org.jasypt.encryption.pbe.config.StringPBEConfig#getStringOutputType()
     */
    private String stringOutputType = "base64";

    public StringEncryptorConfiguration(String pid,Dictionary<String,?> props){
        Map<String,Object> param=new HashMap<>(props.size());
        Enumeration<String> keys = props.keys();
        while (keys.hasMoreElements()){
            String key=keys.nextElement();
            Object value = props.get(key);
            param.put(key, value);
        }
        serviceName=getString(param,"serviceName");
        if (CommonUtils.isEmpty(serviceName)||CommonUtils.isEmpty(serviceName.trim())){
            serviceName=getServiceNameFromPid(pid);
        }
        try {
            stringEncryptorClass= (Class<? extends StringEncryptor>) Class.forName(getString(param,"stringEncryptorClass"));
        } catch (Exception e) {
            log.warn("未找到指定的 stringEncryptorClass=[{}]",getString(param,"stringEncryptorClass"),e);
        }
        try {
            saltGeneratorClass= (Class<? extends SaltGenerator>) Class.forName(getString(param,"saltGeneratorClass"));
        } catch (Exception e) {
            log.warn("未找到指定的 saltGeneratorClass=[{}]",getString(param,"saltGeneratorClass"),e);
        }
        try {
            ivGeneratorClass= (Class<? extends IvGenerator>) Class.forName(getString(param,"ivGeneratorClass"));
        } catch (Exception e) {
            log.warn("未找到指定的 saltGeneratorClass=[{}]",getString(param,"saltGeneratorClass"),e);
        }
        password=getString(param,"password",password);
        algorithm=getString(param,"algorithm",algorithm);
        keyObtentionIterations=getString(param,"keyObtentionIterations",keyObtentionIterations);
        poolSize=getString(param,"poolSize",poolSize);
        providerName=getString(param,"providerName");
        if (CommonUtils.isEmpty(providerName)||CommonUtils.isEmpty(providerName.trim())){
            providerName=null;
        }
        stringOutputType=getString(param,"stringOutputType",stringOutputType);
    }

    public StringEncryptor generate(){
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setStringOutputType(stringOutputType);
        config.setAlgorithm(algorithm);
        config.setPassword(password);
        config.setKeyObtentionIterations(keyObtentionIterations);
        config.setPoolSize(poolSize);
        config.setProviderName(providerName);
        try {
            config.setIvGenerator(ivGeneratorClass.newInstance());
            config.setSaltGenerator(saltGeneratorClass.newInstance());
        } catch (InstantiationException|IllegalAccessException e) {
            log.warn("生成 generator 实例失败",e);
            return null;
        }
        if (PooledPBEStringEncryptor.class.equals(stringEncryptorClass)){
            PooledPBEStringEncryptor encryptor=new PooledPBEStringEncryptor();
            encryptor.setConfig(config);
            return encryptor;
        }
        if (StandardPBEStringEncryptor.class.equals(stringEncryptorClass)){
            StandardPBEStringEncryptor encryptor=new StandardPBEStringEncryptor();
            encryptor.setConfig(config);
            return encryptor;
        }
        return null;
    }

    private String getServiceNameFromPid(String pid) {
       return pid.substring(StringEncryptorConstants.FACTORY_PID.length()+1);
    }
}
