package store.taotao.jasypt.osgi.config;


import org.jasypt.encryption.StringEncryptor;
import org.osgi.framework.ServiceRegistration;

import java.io.Closeable;

public class StringEncryptorRegistration implements Closeable {

    private ServiceRegistration<StringEncryptor> registration;

    public StringEncryptorRegistration(ServiceRegistration<StringEncryptor>  registration) {
        this.registration=registration;
    }

    @Override
    public void close(){
        registration.unregister();
        registration=null;
    }
}
